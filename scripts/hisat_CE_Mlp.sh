#############################
## HISAT2 CE Mlp alignment
#############################

# Software used: hisat2/2.1.0 using 4 threads

BASEDIR=$SCRATCH/pipeline_effectors_paper # path to pipeline_effectors_paper folder
DIR=$BASEDIR/Data/Processed/CEMlp_alignment # where HISAT results will be saved
INDIR=$BASEDIR/Data/Processed/Unmapped_reads # where the unmapped reads results were sav$

for line in {1..45};
# the following 3 lines will create varibles corresponding to each replicate;
do sample=`awk 'NR=='$line'{print $2}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;
sra=`awk 'NR=='$line'{print $1}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;
insert_size=`awk 'NR=='$line'{print $3}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;

echo "Align to Mlp CEs sequences the reads not mapped to TAIR for "$sample"_"$sra"_tair.sam"

hisat2 -X "$insert_size"\
 -p 4\
 -x "$BASEDIR"/metadata/CE_Mlp_hisat\
 -1 "$INDIR"/"$sample"_"$sra"_TU_1.fastq\
 -2 "$INDIR"/"$sample"_"$sra"_TU_2.fastq\
 -S "$DIR"/"$sample"_"$sra"_CE_mlp.sam\
 --summary-file "$DIR"/"$sample"_"$sra"_CE_mlp.txt;

 done
### Above is the code for the hisat alignment
### -p indicates the number of threads used
### -x indicates where the index files are, while -X indicates the size of the fragments (2 read length + mate distance)
### -1 and -2 indicate where the read files are, we used the trimmed reads
### -S is the alignment output (sam file), --summary-file is the summary info, which is also saved in --new-summary as a computer friendly version
