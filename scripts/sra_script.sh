###############################################
## Getting reads from SRA
###############################################

# Software used: sra-toolkit/2.8.2-1

BASEDIR=pipeline_effectors_paper # path to pipeline_effectors_paper folder
DIR=$BASEDIR/Data/Raw_Data

for line in {1..45};
do sample=`awk 'NR=='$line'{print $2}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;
sra=`awk 'NR=='$line'{print $1}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;

echo "Get reads from SRA for sample "$sample" accession "$sra" ";
cd "$DIR";
fastq-dump --split-files $sra -O "$DIR" --gzip;

mv "$sra"_1.fastq.gz "$sample"_"$sra"_1.fastq.gz;
mv "$sra"_2.fastq.gz "$sample"_"$sra"_2.fastq.gz;

done