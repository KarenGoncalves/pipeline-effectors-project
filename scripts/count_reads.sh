# Softwares used: R/3.6 with Bioconductor/3_3.11

###############################################
## count reads aligned to tair
###############################################

BASEDIR=pipeline_effectors_paper # path to pipeline_effectors_paper folder
DIR=$BASEDIR/Data/Processed/Counts/TAIR
INDIR=$BASEDIR/Data/Processed/TAIR_alignment

for line in {1..45};
do sample=`awk 'NR=='$line'{print $2}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;
sra=`awk 'NR=='$line'{print $1}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;

echo "Get reads from SRA for sample "$sample" accession "$sra" "

cd $DIR

Rscript $BASEDIR/scripts/script_countsTAIR.R $INDIR/"$sample"_"$sra"_tair.bam $INDIR/"$sample"_"$sra"_tair.bai "$sample"_"$sra"

###############################################
## count reads aligned to Mlp
###############################################

DIR=$BASEDIR/Data/Processed/Counts/CEMlp
INDIR=$BASEDIR/Data/Processed/CEMlp_alignment

cd $DIR

Rscript $BASEDIR/scripts/script_countsMlp.R $INDIR/"$sample"_"$sra"_CE_mlp.bam $INDIR/"$sample"_"$sra"_CE_mlp.bai "$sample"_"$sra";

done

