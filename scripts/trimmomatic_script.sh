###############################################
## Trimmomatic - Trimming and filtering reads
###############################################

# Softwares samtools/1.10 

BASEDIR=pipeline_effectors_paper
DIR=$BASEDIR/Data/Processed/Trimmed_reads
INDIR=$BASEDIR/Data/Raw_Data
trimmomatic_path=#path to trimmomatic software

for line in {1..45};
do sample=`awk 'NR=='$line'{print $2}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;
sra=`awk 'NR=='$line'{print $1}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;
insert_size=`awk 'NR=='$line'{print $3}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;

cd "$DIR" ;
echo "Trim reads for "$sample"_"$sra" ";

java -jar $trimmomatic_path/trimmomatic-0.36.jar PE\
 -threads 8 -phred33\
 "$INDIR"/"$sample"_"$sra"_1.fastq.gz "$INDIR"/"$sample"_"$sra"_2.fastq.gz\
 "$sample"_"$sra"_p1.fastq.gz "$sample"_"$sra"_u1.fastq.gz\
 "$sample"_"$sra"_p2.fastq.gz "$sample"_"$sra"_u2.fastq.gz\
 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:25;

 done
