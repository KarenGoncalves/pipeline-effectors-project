#############################
## HISAT2 TAIR alignment
#############################

# Software used: hisat2/2.1.0 using 8 threads

BASEDIR=pipeline_effectors_paper # path to pipeline_effectors_paper folder
DIR=$BASEDIR/Data/Processed/TAIR_alignment # where HISAT results will be saved
INDIR=$BASEDIR/Data/Processed/Trimmed_reads # where the trimmomatic results were saved

for line in {1..45};
# the following 3 lines will create varibles corresponding to each replicate;
do sample=`awk 'NR=='$line'{print $2}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;
sra=`awk 'NR=='$line'{print $1}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;
insert_size=`awk 'NR=='$line'{print $3}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;

# Now we create the script for the alignment
hisat2 -X "$insert_size"\
 -p 8\
 -x "$BASEDIR"/metadata/TAIR10_hisat\
 -1 "$INDIR"/"$sample"_"$sra"_p1.fastq.gz\
 -2 "$INDIR"/"$sample"_"$sra"_p2.fastq.gz\
 -S "$DIR"/"$sample"_"$sra"_tair.sam\
 --summary-file "$DIR"/"$sample"_"$sra"_tair.txt;
 
 done

### Above is the code for the hisat alignment
### -p indicates the number of threads used
### -x indicates where the index files are, while -X indicates the size of the fragments (2 read length + mate distance)
### -1 and -2 indicate where the read files are, we used the trimmed reads
### -S is the alignment output (sam file), --summary-file is the summary info, which is also saved in --new-summary as a computer friendly version
