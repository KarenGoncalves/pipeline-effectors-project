# Software used: samtools/1.10  # this lines indicate the softwares used in this script

BASEDIR=pipeline_effectors_paper # path to pipeline_effectors_paper folder;
DIR=$BASEDIR/Data/Processed/Unmapped_reads # where HISAT results will be saved;
INDIR=$BASEDIR/Data/Processed/TAIR_alignment # where the trimmomatic results were saved;

##### in bam, flag 0x4 means read unmapped, and flag 0x8 means mate unmapped ####
#### As we want both, we indicate -f 12 ####

for line in {1..45};
# the following 3 lines will create varibles corresponding to each replicate;
do sample=`awk 'NR=='$line'{print $2}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;
sra=`awk 'NR=='$line'{print $1}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;
insert_size=`awk 'NR=='$line'{print $3}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;

echo "Extract unmapped reads for "$sample"_"$sra"_tair.sam"

samtools view -f 12 --threads 4\
 -b "$INDIR"/"$sample"_"$sra"_tair.sam |\
 samtools bam2fq --threads 4 > "$DIR"/"$sample"_"$sra"_TU.fastq

grep '^@.*/1$' "$DIR"/"$sample"_"$sra"_TU.fastq -A 3 --no-group-separator > "$DIR"/"$sample"_"$sra"_TU_1.fastq
grep '^@.*/2$' "$DIR"/"$sample"_"$sra"_TU.fastq -A 3 --no-group-separator > "$DIR"/"$sample"_"$sra"_TU_2.fastq;

done
