############################################################
##  Convert TAIR alignment files to bam and get indexes
############################################################

# Softwares samtools/1.10 using 4 threads

BASEDIR=pipeline_effectors_paper # path to pipeline_effectors_paper folder;
INDIR=Data/Processed/TAIR_alignment # where the trimmomatic results were saved;


# the following 3 lines will create varibles corresponding to each replicate;

for line in {1..45};
do sample=`awk 'NR=='$line'{print $2}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;
sra=`awk 'NR=='$line'{print $1}' "$BASEDIR"/metadata/Summary_Sra_table.txt`;

echo "Convert TAIR alignment files to bam and get indexes for "$sample"_"$sra"_tair.sam"

samtools sort --threads 4\
 -O BAM "$INDIR"/"$sample"_"$sra"_tair.sam > "$INDIR"/"$sample"_"$sra"_tair.bam

samtools index -b "$INDIR"/"$sample"_"$sra"_tair.bam > "$INDIR"/"$sample"_"$sra"_tair.bai


############################################################
##  Convert CE Mlp alignment files to bam and get indexes
############################################################


INDIR=$BASEDIR/Data/Processed/CEMlp_alignment # where the trimmomatic results were saved;

echo "Convert CE Mlp alignment files to bam and get indexes for "$sample"_"$sra"_tair.sam"

samtools sort --threads 4\
 -O BAM "$INDIR"/"$sample"_"$sra"_CE_mlp.sam > "$INDIR"/"$sample"_"$sra"_CE_mlp.bam

samtools index -b "$INDIR"/"$sample"_"$sra"_CE_mlp.bam > "$INDIR"/"$sample"_"$sra"_CE_mlp.bai;

done


