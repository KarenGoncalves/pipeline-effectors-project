22712810 reads; of these:
  22712810 (100.00%) were paired; of these:
    1466813 (6.46%) aligned concordantly 0 times
    20796554 (91.56%) aligned concordantly exactly 1 time
    449443 (1.98%) aligned concordantly >1 times
    ----
    1466813 pairs aligned concordantly 0 times; of these:
      396634 (27.04%) aligned discordantly 1 time
    ----
    1070179 pairs aligned 0 times concordantly or discordantly; of these:
      2140358 mates make up the pairs; of these:
        1674890 (78.25%) aligned 0 times
        437513 (20.44%) aligned exactly 1 time
        27955 (1.31%) aligned >1 times
96.31% overall alignment rate
