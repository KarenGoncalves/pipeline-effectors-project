16811273 reads; of these:
  16811273 (100.00%) were paired; of these:
    1357272 (8.07%) aligned concordantly 0 times
    15055291 (89.55%) aligned concordantly exactly 1 time
    398710 (2.37%) aligned concordantly >1 times
    ----
    1357272 pairs aligned concordantly 0 times; of these:
      153890 (11.34%) aligned discordantly 1 time
    ----
    1203382 pairs aligned 0 times concordantly or discordantly; of these:
      2406764 mates make up the pairs; of these:
        1869183 (77.66%) aligned 0 times
        520233 (21.62%) aligned exactly 1 time
        17348 (0.72%) aligned >1 times
94.44% overall alignment rate
