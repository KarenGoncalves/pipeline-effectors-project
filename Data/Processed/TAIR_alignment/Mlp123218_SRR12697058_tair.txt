14224041 reads; of these:
  14224041 (100.00%) were paired; of these:
    2256768 (15.87%) aligned concordantly 0 times
    11698273 (82.24%) aligned concordantly exactly 1 time
    269000 (1.89%) aligned concordantly >1 times
    ----
    2256768 pairs aligned concordantly 0 times; of these:
      141222 (6.26%) aligned discordantly 1 time
    ----
    2115546 pairs aligned 0 times concordantly or discordantly; of these:
      4231092 mates make up the pairs; of these:
        3502451 (82.78%) aligned 0 times
        709668 (16.77%) aligned exactly 1 time
        18973 (0.45%) aligned >1 times
87.69% overall alignment rate
