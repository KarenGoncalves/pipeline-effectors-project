13760609 reads; of these:
  13760609 (100.00%) were paired; of these:
    732336 (5.32%) aligned concordantly 0 times
    12716998 (92.42%) aligned concordantly exactly 1 time
    311275 (2.26%) aligned concordantly >1 times
    ----
    732336 pairs aligned concordantly 0 times; of these:
      233264 (31.85%) aligned discordantly 1 time
    ----
    499072 pairs aligned 0 times concordantly or discordantly; of these:
      998144 mates make up the pairs; of these:
        742441 (74.38%) aligned 0 times
        237939 (23.84%) aligned exactly 1 time
        17764 (1.78%) aligned >1 times
97.30% overall alignment rate
