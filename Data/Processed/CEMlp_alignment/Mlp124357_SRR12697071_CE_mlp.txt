245884 reads; of these:
  245884 (100.00%) were paired; of these:
    245268 (99.75%) aligned concordantly 0 times
    578 (0.24%) aligned concordantly exactly 1 time
    38 (0.02%) aligned concordantly >1 times
    ----
    245268 pairs aligned concordantly 0 times; of these:
      4 (0.00%) aligned discordantly 1 time
    ----
    245264 pairs aligned 0 times concordantly or discordantly; of these:
      490528 mates make up the pairs; of these:
        490270 (99.95%) aligned 0 times
        250 (0.05%) aligned exactly 1 time
        8 (0.00%) aligned >1 times
0.30% overall alignment rate
