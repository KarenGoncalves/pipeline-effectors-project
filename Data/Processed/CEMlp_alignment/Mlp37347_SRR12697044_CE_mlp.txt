470651 reads; of these:
  470651 (100.00%) were paired; of these:
    461588 (98.07%) aligned concordantly 0 times
    7760 (1.65%) aligned concordantly exactly 1 time
    1303 (0.28%) aligned concordantly >1 times
    ----
    461588 pairs aligned concordantly 0 times; of these:
      49 (0.01%) aligned discordantly 1 time
    ----
    461539 pairs aligned 0 times concordantly or discordantly; of these:
      923078 mates make up the pairs; of these:
        911805 (98.78%) aligned 0 times
        10944 (1.19%) aligned exactly 1 time
        329 (0.04%) aligned >1 times
3.13% overall alignment rate
