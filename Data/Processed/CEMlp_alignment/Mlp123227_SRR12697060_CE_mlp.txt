361696 reads; of these:
  361696 (100.00%) were paired; of these:
    358553 (99.13%) aligned concordantly 0 times
    2948 (0.82%) aligned concordantly exactly 1 time
    195 (0.05%) aligned concordantly >1 times
    ----
    358553 pairs aligned concordantly 0 times; of these:
      36 (0.01%) aligned discordantly 1 time
    ----
    358517 pairs aligned 0 times concordantly or discordantly; of these:
      717034 mates make up the pairs; of these:
        716164 (99.88%) aligned 0 times
        797 (0.11%) aligned exactly 1 time
        73 (0.01%) aligned >1 times
1.00% overall alignment rate
