648237 reads; of these:
  648237 (100.00%) were paired; of these:
    644643 (99.45%) aligned concordantly 0 times
    3335 (0.51%) aligned concordantly exactly 1 time
    259 (0.04%) aligned concordantly >1 times
    ----
    644643 pairs aligned concordantly 0 times; of these:
      35 (0.01%) aligned discordantly 1 time
    ----
    644608 pairs aligned 0 times concordantly or discordantly; of these:
      1289216 mates make up the pairs; of these:
        1287283 (99.85%) aligned 0 times
        1841 (0.14%) aligned exactly 1 time
        92 (0.01%) aligned >1 times
0.71% overall alignment rate
