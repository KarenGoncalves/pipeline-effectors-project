# Pipeline from raw reads to differential expression analysis

Scripts used to obtain results in the preprint:

Dos Santos, K.C.G.; Pelletier, G.; Seguin, A.; Hawkes, J.A.; Guillemette, F.; Desgagne-Penix, I.; Germain, H. Differential alteration of plant functions by homologous fungal effectors. bioRxiv, https://doi.org/10.1101/2020.10.30.363010, 2020. 

## Files

Raw reads and count matrices are available in NCBI GEO under accession GSE158410.

## Software versions

nixpkgs/16.09 

gcccore/.5.4.0

icc/.2016.4.258

ifort/.2016.4.258

intel/2016.4

python/2.7.14

java/1.8.0_121

sra-toolkit/2.8.2-1

trimmomatic/0.36

hisat2/2.1.0

samtools/1.10

R/3.6.3

Bioconductor/3.11


